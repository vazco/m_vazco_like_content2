'use strict';

Meteor.methods({
    likeContent: function (settings) {
        this.unblock();
        var user = Meteor.user(),
            query = {},
            update = {},
            mod,
            path,
            content,
            result;
        settings = settings || {};
        if (user && Vazco.get(global, settings.collection) && settings._id) {
            // If unlike flag is set, unlike content
            if (settings.unlike) {
                mod = update.$pull = {};
            } else {
                mod = update.$addToSet = {};
            }

            // This part will make some complicated mongo query if path and parentDocId is set
            // It makes liking embedded documents possible.
            // Right now it only work with arrays of objects.
            // TODO: check whether this can be use by bad people to do vicious things.
            if (settings.path && settings.parentDocId) {
                query._id = settings.parentDocId;
                query[settings.path + '._id'] = settings._id;
                path = settings.path + '.$.likes';
            } else {
                // If not, just modify main document
                query._id = settings._id;
                path = 'likes';
            }

            if(settings.custom){
                path = settings.custom + '.likes';
            }

            // At the end just put user id in [likes] array
            mod[path] = user._id;
            result = Vazco.get(global, settings.collection).update(query, update);

            // Temporary after hook for myQilin.
            // TODO: General purpose hooks for Universe.
            //if (result && MyQilin && MyQilin.likeHook) {
            //    MyQilin.likeHook(user._id, settings);
            //}

            return result;
        }
    }
});
