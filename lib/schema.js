'use strict';

likeSchema = new SimpleSchema({
        likes: {
            type: [String],
            optional: true
        }
    }
);
