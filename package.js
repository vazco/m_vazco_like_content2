'use strict';

Package.describe({
    summary: 'Vazco Like Content'
});

Package.on_use(function (api) {
    api.use([
        'templating',
        'handlebars',
        'jquery',
        'underscore'
    ], [
        'client'
    ]);

    api.use([
        'less',
        'aldeed:simple-schema'
    ], [
        'client',
        'server'
    ]);

    api.add_files([
        'lib/schema.js'
    ], [
        'client',
        'server'
    ]);

    api.add_files([
        'client/template.html',
        'client/template.less',
        'client/template.js'
    ], [
        'client'
    ]);

    api.add_files([
        'server/methods.js'
    ], [
        'server'
    ]);

    api.export([
        'likeSchema'
    ], [
        'client',
        'server'
    ]);
});