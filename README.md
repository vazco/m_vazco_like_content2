#Vazco like content2

###WARNING: This package will allow every user to add 'likes' array to every document (and sub document) in every collection in project. This is probably what you want, but I am warning you anyway.

##What is it?

This package give you like button you can put on any document. It is simple one-click-to-like, one-click-to-unlike mechanism.

##How to use it?

Just add likeButtons template to template where you want to display button.

You also need to provide document to like and collection name. Collection name must be a string that corresponds to global variable server can access by global[collectionName].

````
{{> likeButtons collection='collectionName' doc=documentObject}}
````
Object **doc** - document object.

String **collection** - string that corresponds to global property with collection (just basic meteor way to declare collections in project, nothing complicated).

If you use collection2 extend schema like this:

```js
MyCollection = new Meteor.Collection('MyCollection', {
    schema: new SimpleSchema([
        likeSchema, // <- important part
        {
            //rest of my fields
        }
    ])
});
```

##How to like embeded document?

It is possible to like embeded documents. It is implemented for likes in comments etc. 

Container document looks like this:
````
{
    _id: 'id of container document', // <- this goes to 'parentDocId'
    comments: [ // <- this goes to 'path' (name of property as a string)
            {_id: 'id', comment: 'comment'}, // <- this goes to 'doc' (object)
            {_id: 'id2', comment: 'comment2'}
    ]
}
````

Template:
````
{{> likeButtons collection='CollectionOfMainDocument' parentDocId='id of container document' path='comments' doc=commentDocument}}
````