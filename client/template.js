'use strict';

Template.likeButtons.events({
    'click .js-btn-like': function () {
        if (this.doc && this.collection) {
            var settings = {
                _id: this.doc._id,
                collection: this.collection
            };

            if (this.customPath){
                settings.custom = this.customPath;
            }

            if (this.path && this.parentDocId){
                settings.path = this.path;
                settings.parentDocId = this.parentDocId;
            }

            if (isLiked.call(this)) {
                settings.unlike = true;
            }

            Meteor.call('likeContent', settings);
        }
    }
});

Template.likeButtons.helpers({
    count: function () {
        if (this.doc && _.isArray(this.doc.likes)) {
            return this.doc.likes.length;
        }
        if (this.doc && this.customPath && _.isArray(this.doc[this.customPath].likes)){
            return this.doc[this.customPath].likes.length;
        }

        return 0;
    },

    isLiked: isLiked
});

var isLiked = function () {
    if (this.doc && this.doc.likes) {
        return _.contains(this.doc.likes, Meteor.userId());
    }
    if (this.doc && this.customPath && _.isArray(this.doc[this.customPath].likes)){
        return _.contains(this.doc[this.customPath].likes, Meteor.userId());
    }
};